package com.testmatick;

import com.testmatick.shapes.Shape;
import com.testmatick.shapes.generator.ShapesGenerator;

import java.util.List;

public class Application {
    public static void main(String[] args) {
        List<Shape> listShapes = ShapesGenerator.getRandomShapesList(5);
        for (Shape shape : listShapes) {
            System.out.println(shape.draw());
        }
    }
}
