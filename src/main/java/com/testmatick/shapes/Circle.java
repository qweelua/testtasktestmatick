package com.testmatick.shapes;

public class Circle implements Shape {
    private float radius;
    private Color color;

    public Circle(float radius, Color color) {
        this.radius = radius;
        this.color = color;
    }

    public float getRadius() {
        return radius;
    }

    @Override
    public float calculateArea() {
        return (float) (radius * 3.14);
    }

    @Override
    public String getColor() {
        return color.getName();
    }

    @Override
    public String draw() {
        return "Фигура: круг, площадь " + calculateArea() + " кв. ед., радиус: "
                + getRadius() + " ед. цвет: " + getColor();
    }
}
