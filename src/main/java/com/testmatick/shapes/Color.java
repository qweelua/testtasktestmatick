package com.testmatick.shapes;

public enum Color {
    WHITE("белый"), RED("красный"), BLACK("черный"),
    YELLOW("желтый"), BLUE("синий"), ORANGE("оранжевый");

    private String name;

    Color(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
