package com.testmatick.shapes;

public interface Shape {
    float calculateArea();

    String getColor();

    String draw();
}
