package com.testmatick.shapes;

public class Square implements Shape {
    private float sideA;
    private Color color;

    public Square(float sideA, Color color) {
        this.sideA = sideA;
        this.color = color;
    }

    @Override
    public float calculateArea() {
        return sideA * sideA;
    }

    @Override
    public String getColor() {
        return color.getName();
    }

    public float getSideA() {
        return sideA;
    }

    @Override
    public String draw() {
        return "Фигура: квадрат, площадь " + calculateArea() + " кв. ед., сторона: "
                + getSideA() + " ед. цвет: " + getColor();
    }
}
