package com.testmatick.shapes;

public class Trapeze implements Shape {
    private float sideA;
    private float sideB;
    private float altitude;
    private Color color;

    public Trapeze(float sideA, float sideB, float altitude, Color color) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.altitude = altitude;
        this.color = color;
    }

    public float getAltitude() {
        return altitude;
    }

    @Override
    public float calculateArea() {
        return (float) (0.5 * (sideA + sideB) * altitude);
    }

    @Override
    public String getColor() {
        return color.getName();
    }

    @Override
    public String draw() {
        return "Фигура: трапеция, площадь " + calculateArea() + " кв. ед., высота: "
                + getAltitude() + " ед. цвет: " + getColor();
    }
}
