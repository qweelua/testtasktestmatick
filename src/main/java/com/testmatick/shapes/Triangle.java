package com.testmatick.shapes;

public class Triangle implements Shape {
    private float sideA;
    private float sideB;
    private float sideC;
    private float hypotenuse;
    private Color color;

    public Triangle(float sideA, float sideB, float sideC, float hypotenuse, Color color) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
        this.hypotenuse = hypotenuse;
        this.color = color;
    }

    @Override
    public float calculateArea() {
        double halfPerimeter = ((sideA + sideB + sideC) * 0.5);
        return (float) Math.sqrt(halfPerimeter * ((halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC)));
    }

    @Override
    public String getColor() {
        return color.getName();
    }

    public float getHypotenuse() {
        return hypotenuse;
    }

    @Override
    public String draw() {
        return "Фигура: треугольник, площадь " + calculateArea() + " кв. ед., гипотенуза: "
                + getHypotenuse() + " ед. цвет: " + getColor();
    }
}
