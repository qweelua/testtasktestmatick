package com.testmatick.shapes.generator;

import com.testmatick.shapes.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ShapesGenerator {
    private static Random random = new Random();

    public static List<Shape> getRandomShapesList(int count) {
        List<Shape> shapes = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Shape randomShape = getRandomShape();
            shapes.add(randomShape);
        }
        return shapes;
    }

    public static Shape getRandomShape() {
        int randomDigit = random.nextInt(4);
        Color[] colors = Color.values();
        switch (randomDigit) {
            case 0:
                return new Circle(getRandomFloat(), colors[random.nextInt(5)]);
            case 1:
                return new Square(getRandomFloat(), colors[random.nextInt(5)]);
            case 2:
                return new Trapeze(getRandomFloat(), getRandomFloat(), getRandomFloat(), colors[random.nextInt(5)]);
            case 3:
                return new Triangle(getRandomFloat(), getRandomFloat(), getRandomFloat(), getRandomFloat(),
                        colors[random.nextInt(5)]);
            default:
                return null;
        }
    }

    private static float getRandomFloat() {
        return 5 + (random.nextFloat() * (5));
    }
}
